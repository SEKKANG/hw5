package csb.gui;

import static csb.CSB_StartupConstants.*;
import csb.CSB_PropertyType;
import csb.controller.CourseEditController;
import csb.data.Course;
import csb.data.CourseDataManager;
import csb.data.CourseDataView;
import csb.data.CoursePage;
import csb.controller.FileController;
import csb.controller.ScheduleEditController;
import csb.data.PlayerItem;
import csb.data.Semester;
import csb.file.CourseFileManager;
import csb.file.CourseSiteExporter;
import csb.file.JsonCourseFileManager;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * This class provides the Graphical User Interface for this application,
 * managing all the UI components for editing a Course and exporting it to a
 * site.
 *
 * @author Richard McKenna
 */
public class CSB_GUI implements CourseDataView {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS GUI'S COMPONENTS TO A STYLE SHEET THAT IT USES

    static final String PRIMARY_STYLE_SHEET = PATH_CSS + "csb_style.css";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_SUBJECT_PANE = "subject_pane";
    
    static final String CLASS_NAME_PANE = "name_pane";
    
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_SUBHEADING_LABEL = "subheading_label";
    static final String CLASS_PROMPT_LABEL = "prompt_label";
    static final String EMPTY_TEXT = "";
    static final int LARGE_TEXT_FIELD_LENGTH = 20;
    static final int SMALL_TEXT_FIELD_LENGTH = 5;

    // THIS MANAGES ALL OF THE APPLICATION'S DATA
    CourseDataManager dataManager;

    // THIS MANAGES COURSE FILE I/O
    CourseFileManager courseFileManager;

    // THIS MANAGES EXPORTING OUR SITE PAGES
    CourseSiteExporter siteExporter;

    // THIS HANDLES INTERACTIONS WITH FILE-RELATED CONTROLS
    FileController fileController;

    
    // THIS HANDLES INTERACTIONS WITH COURSE INFO CONTROLS
    CourseEditController courseController;
    
    // THIS HANDLES REQUESTS TO ADD OR EDIT SCHEDULE STUFF
    ScheduleEditController scheduleController;

    // THIS IS THE APPLICATION WINDOW
    Stage primaryStage;

    // THIS IS THE STAGE'S SCENE GRAPH
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane csbPane;
    
    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newCourseButton;
    Button loadCourseButton;
    Button saveCourseButton;
    Button exportSiteButton;
    Button exitButton;
    
    //THIS IS THE BOTTOM TOOLBAR AND ITS CONTROLS
    FlowPane bottomToolbarPane;
    Button homeButton;
    Button playersButton;
    Button standingButton;
    Button draftButton;
    Button mlbteamsButton;
    
    // WE'LL ORGANIZE OUR WORKSPACE COMPONENTS USING A BORDER PANE
    BorderPane workspacePane;
    boolean workspaceActivated;
    
    // WE'LL PUT THE WORKSPACE INSIDE A SCROLL PANE
    BorderPane workspaceScrollPane;

    // WE'LL PUT THIS IN THE TOP OF THE WORKSPACE, IT WILL
    // HOLD TWO OTHER PANES FULL OF CONTROLS AS WELL AS A LABEL
    VBox topWorkspacePane;
    Label courseHeadingLabel;
    SplitPane topWorkspaceSplitPane;

    //THESE ARE THE CONTROLS FOR THE DEFAULT SCREEN PAGE
    TableView<PlayerItem> playersTable;  
    TableColumn firstColumn;
            TableColumn lastColumn;
            TableColumn proColumn;
            TableColumn positionColumn;
            TableColumn birthColumn;
            TableColumn rColumn;
            TableColumn hrColumn;
            TableColumn rbiColumn;
            TableColumn sbColumn;
            TableColumn brColumn;
            TableColumn estimatedColumn;
            TableColumn noteColumn;
TableColumn nationColumn;

    VBox playersPane;
    HBox playerAddRemovePane;
    HBox playerPositionPane;
    VBox playerListPane;
    Button addPlayerButton;
    Button removePlayerButton;
    Label playersSearchLabel;
    TextField playersSearchField;
    RadioButton allRadioButton;
    RadioButton cRadioButton;
    RadioButton firstRadioButton;
    RadioButton clRadioButton;
    RadioButton thirdRadioButton;
    RadioButton secondRadioButton;
    RadioButton miRadioButton;
    RadioButton ssRadioButton;
    RadioButton ofRadioButton;
    RadioButton uRadioButton;
    RadioButton pRadioButton;
    
    // THESE ARE THE CONTROLS FOR THE BASIC SCHEDULE PAGE HEADER INFO
   
    
    
    // THIS REGION IS FOR MANAGING HWs
   
    // AND TABLE COLUMNS
    static final String COL_DESCRIPTION = "Description";
    static final String COL_DATE = "Date";
    static final String COL_LINK = "Link";
    static final String COL_TOPIC = "Topic";
    static final String COL_SESSIONS = "Number of Sessions";
    static final String COL_NAME = "Name";
    static final String COL_TOPICS = "Topics";
    
    static final String COL_FIRST = "First";
    static final String COL_LAST = "Last";
    static final String COL_TEAM = "Pro Team";
    static final String COL_POSITION = "Positions";
    static final String COL_BIRTH = "Year of Birth";
    static final String COL_R_W = "R/W";
    static final String COL_HR_SV = "HR/SV";
    static final String COL_RBI_K = "RBI/K";
    static final String COL_SB_ERA = "SB/ERA";
    static final String COL_BA_WHIP = "BA/WHIP";
    static final String COL_E_VALUE = "Estimated Value";
    
    static final String COL_NOTE ="Notes";
    static final String COL_NATION ="Nation";
            
    // HERE ARE OUR DIALOGS
    MessageDialog messageDialog;
    YesNoCancelDialog yesNoCancelDialog;
    ProgressDialog progressDialog;
    
    /**
     * Constructor for making this GUI, note that it does not initialize the UI
     * controls. To do that, call initGUI.
     *
     * @param initPrimaryStage Window inside which the GUI will be displayed.
     */
    public CSB_GUI(Stage initPrimaryStage) {
        primaryStage = initPrimaryStage;
    }

    /**
     * Accessor method for the data manager.
     *
     * @return The CourseDataManager used by this UI.
     */
    public CourseDataManager getDataManager() {
        return dataManager;
    }

    /**
     * Accessor method for the file controller.
     *
     * @return The FileController used by this UI.
     */
    public FileController getFileController() {
        return fileController;
    }

    /**
     * Accessor method for the course file manager.
     *
     * @return The CourseFileManager used by this UI.
     */
    public CourseFileManager getCourseFileManager() {
        return courseFileManager;
    }

    /**
     * Accessor method for the site exporter.
     *
     * @return The CourseSiteExporter used by this UI.
     */
    public CourseSiteExporter getSiteExporter() {
        return siteExporter;
    }

    /**
     * Accessor method for the window (i.e. stage).
     *
     * @return The window (i.e. Stage) used by this UI.
     */
    public Stage getWindow() {
        return primaryStage;
    }
    
    public MessageDialog getMessageDialog() {
        return messageDialog;
    }
    
    public YesNoCancelDialog getYesNoCancelDialog() {
        return yesNoCancelDialog;
    }

    /**
     * Mutator method for the data manager.
     *
     * @param initDataManager The CourseDataManager to be used by this UI.
     */
    public void setDataManager(CourseDataManager initDataManager) {
        dataManager = initDataManager;
    }

    /**
     * Mutator method for the course file manager.
     *
     * @param initCourseFileManager The CourseFileManager to be used by this UI.
     */
    public void setCourseFileManager(CourseFileManager initCourseFileManager) {
        courseFileManager = initCourseFileManager;
    }

    /**
     * Mutator method for the site exporter.
     *
     * @param initSiteExporter The CourseSiteExporter to be used by this UI.
     */
    public void setSiteExporter(CourseSiteExporter initSiteExporter) {
        siteExporter = initSiteExporter;
    }

    /**
     * This method fully initializes the user interface for use.
     *
     * @param windowTitle The text to appear in the UI window's title bar.
     * @param subjects The list of subjects to choose from.
     * @throws IOException Thrown if any initialization files fail to load.
     */
    public void initGUI(String windowTitle, ArrayList<String> subjects) throws IOException {
        // INIT THE DIALOGS
        initDialogs();
        
        // INIT THE TOOLBAR
        initFileToolbar();

        bottomToolbar();
        
        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
        // TO THE WINDOW YET
        initWorkspace(subjects);

        // NOW SETUP THE EVENT HANDLERS
        initEventHandlers();

        // AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
        initWindow(windowTitle);
    }

    /**
     * When called this function puts the workspace into the window,
     * revealing the controls for editing a Course.
     */
    public void activateWorkspace() {
        if (!workspaceActivated) {
            // PUT THE WORKSPACE IN THE GUI
            csbPane.setCenter(workspaceScrollPane);
            workspaceActivated = true;
        }
    }
    
    /**
     * This function takes all of the data out of the courseToReload 
     * argument and loads its values into the user interface controls.
     * 
     * @param courseToReload The Course whose data we'll load into the GUI.
     */
    @Override
    public void reloadCourse(Course courseToReload) {
        // FIRST ACTIVATE THE WORKSPACE IF NECESSARY
        if (!workspaceActivated) {
            activateWorkspace();
        }

        // WE DON'T WANT TO RESPOND TO EVENTS FORCED BY
        // OUR INITIALIZATION SELECTIONS
        //courseController.enable(false);

        // FIRST LOAD ALL THE BASIC COURSE INFO
      
        
        // THE SCHEDULE ITEMS TABLE
       
        // THE LECTURES TABLE
        
        // THE HWS TABLE

        // NOW WE DO WANT TO RESPOND WHEN THE USER INTERACTS WITH OUR CONTROLS
        //courseController.enable(true);
    }

    /**
     * This method is used to activate/deactivate toolbar buttons when
     * they can and cannot be used so as to provide foolproof design.
     * 
     * @param saved Describes whether the loaded Course has been saved or not.
     */
    public void updateToolbarControls(boolean saved) {
        // THIS TOGGLES WITH WHETHER THE CURRENT COURSE
        // HAS BEEN SAVED OR NOT
        saveCourseButton.setDisable(saved);

        // ALL THE OTHER BUTTONS ARE ALWAYS ENABLED
        // ONCE EDITING THAT FIRST COURSE BEGINS
        loadCourseButton.setDisable(false);
        exportSiteButton.setDisable(false);

        // NOTE THAT THE NEW, LOAD, AND EXIT BUTTONS
        // ARE NEVER DISABLED SO WE NEVER HAVE TO TOUCH THEM
    }

    /**
     * This function loads all the values currently in the user interface
     * into the course argument.
     * 
     * @param course The course to be updated using the data from the UI controls.
     */
 

    /****************************************************************************/
    /* BELOW ARE ALL THE PRIVATE HELPER METHODS WE USE FOR INITIALIZING OUR GUI */
    /****************************************************************************/
    
    private void initDialogs() {
        messageDialog = new MessageDialog(primaryStage, CLOSE_BUTTON_LABEL);
        yesNoCancelDialog = new YesNoCancelDialog(primaryStage);
        progressDialog = new ProgressDialog(primaryStage, "Preparing");
    }
    
    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
        fileToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        newCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.NEW_COURSE_ICON, CSB_PropertyType.NEW_COURSE_TOOLTIP, false);
        loadCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.LOAD_COURSE_ICON, CSB_PropertyType.LOAD_COURSE_TOOLTIP, false);
        saveCourseButton = initChildButton(fileToolbarPane, CSB_PropertyType.SAVE_COURSE_ICON, CSB_PropertyType.SAVE_COURSE_TOOLTIP, true);
        exportSiteButton = initChildButton(fileToolbarPane, CSB_PropertyType.EXPORT_PAGE_ICON, CSB_PropertyType.EXPORT_PAGE_TOOLTIP, true);
        exitButton = initChildButton(fileToolbarPane, CSB_PropertyType.EXIT_ICON, CSB_PropertyType.EXIT_TOOLTIP, false);
    }

    private void bottomToolbar() {
        bottomToolbarPane = new FlowPane();

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
        // START AS ENABLED (false), WHILE OTHERS DISABLED (true)
        
       homeButton = initChildButton(bottomToolbarPane, CSB_PropertyType.HOME_ICON, CSB_PropertyType.HOME_TOOLTIP, false);
       playersButton = initChildButton(bottomToolbarPane, CSB_PropertyType.PLAYERS_ICON, CSB_PropertyType.PLAYERS_TOOLTIP, false);
       standingButton = initChildButton(bottomToolbarPane, CSB_PropertyType.FANTASY_STANDING_ICON, CSB_PropertyType.FANTASY_STANDING_TOOLTIP, false);
       draftButton = initChildButton(bottomToolbarPane, CSB_PropertyType.DRAFT_ICON, CSB_PropertyType.DRAFT_TOOLTIP, false);
       mlbteamsButton= initChildButton(bottomToolbarPane, CSB_PropertyType.MLB_TEAMS_ICON, CSB_PropertyType.MLB_TEAMS_TOOLTIP, false);
    }

    
    // CREATES AND SETS UP ALL THE CONTROLS TO GO IN THE APP WORKSPACE
    private void initWorkspace(ArrayList<String> subjects) throws IOException {
        // THE WORKSPACE HAS A FEW REGIONS, THIS 
        // IS FOR BASIC COURSE EDITING CONTROLS
        initBasicCourseInfoControls(subjects);

        // THIS IS FOR SELECTING PAGE LINKS TO INCLUDE
        initPageSelectionControls();

        // THE TOP WORKSPACE HOLDS BOTH THE BASIC COURSE INFO
        // CONTROLS AS WELL AS THE PAGE SELECTION CONTROLS
        initTopWorkspace();

        // THIS IS FOR MANAGING SCHEDULE EDITING
        initScheduleItemsControls();

        // THIS HOLDS ALL OUR WORKSPACE COMPONENTS, SO NOW WE MUST
        // ADD THE COMPONENTS WE'VE JUST INITIALIZED
        workspacePane = new BorderPane();
        workspacePane.setTop(topWorkspacePane);
        workspacePane.setCenter(playersPane);
        workspacePane.setBottom(bottomToolbarPane);
        
        workspacePane.getStyleClass().add(CLASS_BORDERED_PANE);
        
        // AND NOW PUT IT IN THE WORKSPACE
        workspaceScrollPane = new BorderPane();
        workspaceScrollPane.setCenter(workspacePane);
         
       // workspaceScrollPane.setFitToWidth(true);

        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;
    }
    
    // INITIALIZES THE TOP PORTION OF THE WORKWPACE UI
    private void initTopWorkspace() {
        // HERE'S THE SPLIT PANE, ADD THE TWO GROUPS OF CONTROLS
        topWorkspaceSplitPane = new SplitPane();
       

        // THE TOP WORKSPACE PANE WILL ONLY DIRECTLY HOLD 2 THINGS, A LABEL
        // AND A SPLIT PANE, WHICH WILL HOLD 2 ADDITIONAL GROUPS OF CONTROLS
        topWorkspacePane = new VBox();
        playersPane = new VBox();
        
        playerAddRemovePane = new HBox();
        playersPane.getChildren().add(playerAddRemovePane);
       
    
        addPlayerButton = initChildButton(playerAddRemovePane, CSB_PropertyType.ADD_ICON, CSB_PropertyType.ADD_ITEM_TOOLTIP, false);
        removePlayerButton = initChildButton(playerAddRemovePane, CSB_PropertyType.MINUS_ICON, CSB_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        
        playersSearchLabel  = initLabel(CSB_PropertyType.PLAYERS_SEARCH_LABEL, CLASS_SUBHEADING_LABEL);
        playerAddRemovePane.getChildren().add(playersSearchLabel);
        playersSearchField = initChildTextField(playerAddRemovePane, CSB_PropertyType.MINUS_ICON, CSB_PropertyType.REMOVE_ITEM_TOOLTIP, false);
        
        /*  ToggleGroup group = new ToggleGroup();
    RadioButton button1 = new RadioButton("select first");
    button1.setToggleGroup(group);
    button1.setSelected(true);
    RadioButton button2 = new RadioButton("select second");
    button2.setToggleGroup(group);*/
        playerPositionPane = new HBox();
        
        playersPane.getChildren().add(playerPositionPane);
        ToggleGroup group = new ToggleGroup();
        RadioButton radioAll = new RadioButton("All");
        radioAll.setToggleGroup(group);
        radioAll.setSelected(true);
        RadioButton radioC = new RadioButton("C");
        radioC.setToggleGroup(group);
        radioC.setSelected(false);
        RadioButton radio1B = new RadioButton("1B");
        radio1B.setToggleGroup(group);
        radio1B.setSelected(false);
        RadioButton radioCI = new RadioButton("CI");
        radioCI.setToggleGroup(group);
        radioCI.setSelected(false);
        RadioButton radio3B = new RadioButton("3B");
        radio3B.setToggleGroup(group);
        radio3B.setSelected(false);
        RadioButton radio2B = new RadioButton("2B");
        radio2B.setToggleGroup(group);
        radio2B.setSelected(false);
        RadioButton radioMI = new RadioButton("MI");
        radioMI.setToggleGroup(group);
        radioMI.setSelected(false);
        RadioButton radioSS = new RadioButton("SS");
        radioSS.setToggleGroup(group);
        radioSS.setSelected(false);
        RadioButton radioOF = new RadioButton("OF");
        radioOF.setToggleGroup(group);
        radioOF.setSelected(false);
        RadioButton radioU = new RadioButton("U");
        radioU.setToggleGroup(group);
        radioU.setSelected(false);
        RadioButton radioP = new RadioButton("P");
        radioP.setToggleGroup(group);
        radioP.setSelected(false);
        
        playerPositionPane.getChildren().add(radioAll);
        playerPositionPane.getChildren().add(radioC);
        playerPositionPane.getChildren().add(radio1B);
        playerPositionPane.getChildren().add(radioCI);
        playerPositionPane.getChildren().add(radio3B);
        playerPositionPane.getChildren().add(radio2B);
        playerPositionPane.getChildren().add(radioMI);
        playerPositionPane.getChildren().add(radioSS);
        playerPositionPane.getChildren().add(radioOF);
        playerPositionPane.getChildren().add(radioU);
        playerPositionPane.getChildren().add(radioP);
        
        
        playersTable = new TableView();
        playersPane.getChildren().add(playersTable);
        
       
         
        firstColumn = new TableColumn(COL_FIRST);
        firstColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("first"));
        playersTable.getColumns().add(firstColumn);
        
        lastColumn = new TableColumn(COL_LAST);
        lastColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("last"));
        playersTable.getColumns().add(lastColumn);
        
        proColumn = new TableColumn(COL_TEAM);
        proColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("proTeam"));
        playersTable.getColumns().add(proColumn);
        
        positionColumn = new TableColumn(COL_POSITION);
        positionColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("position"));
        playersTable.getColumns().add(positionColumn);
        
        birthColumn = new TableColumn(COL_BIRTH);
        birthColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("birthYear"));
        playersTable.getColumns().add(birthColumn);
        
        rColumn = new TableColumn(COL_R_W);
        rColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("rw"));
        playersTable.getColumns().add(rColumn);
        
        hrColumn = new TableColumn(COL_HR_SV);
        hrColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("hr"));
        playersTable.getColumns().add(hrColumn);
        
        rbiColumn = new TableColumn(COL_RBI_K);
        rbiColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("rbi"));
        playersTable.getColumns().add(rbiColumn);
        
        sbColumn = new TableColumn(COL_SB_ERA);
        sbColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("sb"));
        playersTable.getColumns().add(sbColumn);
        
        brColumn = new TableColumn(COL_BA_WHIP);
        brColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("ba"));
        playersTable.getColumns().add(brColumn);
        
        estimatedColumn = new TableColumn(COL_E_VALUE);
        estimatedColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("estimated"));
        playersTable.getColumns().add(estimatedColumn);
        
        noteColumn = new TableColumn(COL_NOTE);
        noteColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("note"));
        playersTable.getColumns().add(noteColumn);
        
        nationColumn = new TableColumn(COL_NATION);
        nationColumn.setCellValueFactory(new PropertyValueFactory<PlayerItem, String>("nation"));
        playersTable.getColumns().add(nationColumn);
        
     
        //playersTable.setItems(dataManager.getCourse().getScheduleItems());
        
            
        //// jSON PARSING START
        try {
            JsonCourseFileManager jsonFileManager = new JsonCourseFileManager();
            String JSON_FILE_PATH_HITTERS = PATH_DATA + "Hitters.json";
            ArrayList<PlayerItem> hitters = jsonFileManager.loadHitters(JSON_FILE_PATH_HITTERS);
            ObservableList<PlayerItem> list_players;
            list_players = FXCollections.observableArrayList();
            
            int ssssss = hitters.size();
            
            for(int i = 0; i < hitters.size(); i++) {
                list_players.add(hitters.get(i));
            }
            
            playersTable.setItems(list_players);
            
            
        
        
        }
        catch(Exception ex) {
           
        }
        
        
        
        ///// JSON PARSING END
        
        
        
        
         
         //firstColumn.setCellValueFactory(new PropertyValueFactory<String, String>("first"));
       
         
        topWorkspacePane.getStyleClass().add(CLASS_NAME_PANE);

        // HERE'S THE LABEL
        courseHeadingLabel = initChildLabel(topWorkspacePane, CSB_PropertyType.COURSE_HEADING_LABEL, CLASS_HEADING_LABEL);

        // AND NOW ADD THE SPLIT PANE
        topWorkspacePane.getChildren().add(topWorkspaceSplitPane);
    }

    // INITIALIZES THE CONTROLS IN THE LEFT HALF OF THE TOP WORKSPACE
    private void initBasicCourseInfoControls(ArrayList<String> subjects) throws IOException {
        // THESE ARE THE CONTROLS FOR THE BASIC SCHEDULE PAGE HEADER INFO
        // WE'LL ARRANGE THEM IN THE LEFT SIDE IN A VBox
        
        ObservableList<String> semesterChoices = FXCollections.observableArrayList();
        for (Semester s : Semester.values()) {
            semesterChoices.add(s.toString());
        }
        

        // THEN THE COURSE YEAR
      
        ObservableList<Integer> yearChoices = FXCollections.observableArrayList();
        for (int i = LocalDate.now().getYear(); i <= LocalDate.now().getYear() + 1; i++) {
            yearChoices.add(i);
        }
        

        // THEN THE COURSE TITLE
        

        // THEN THE INSTRUCTOR NAME
       

        // AND THE INSTRUCTOR HOMEPAGE
      
    }

    // INITIALIZES THE CONTROLS IN THE RIGHT HALF OF THE TOP WORKSPACE
    private void initPageSelectionControls() {
        // THESE ARE THE CONTROLS FOR SELECTING WHICH PAGES THE SCHEDULE
        // PAGE WILL HAVE TO LINK TO
        
    }
    
    // INITIALIZE THE SCHEDULE ITEMS CONTROLS
    private void initScheduleItemsControls() {
        // FOR THE LEFT
        

        // THIS ONE IS ON THE RIGHT
       

        // THIS SPLITS THE TOP
       
        // NOW THE CONTROLS FOR ADDING SCHEDULE ITEMS
       
        
        // AND NOW THE CONTROLS FOR ADDING HWS
        
      
        // NOW LET'S ASSEMBLE ALL THE CONTAINERS TOGETHER

        // THIS IS FOR STUFF IN THE TOP OF THE SCHEDULE PANE, WE NEED TO PUT TWO THINGS INSIDE
       
    }

    // INITIALIZE THE WINDOW (i.e. STAGE) PUTTING ALL THE CONTROLS
    // THERE EXCEPT THE WORKSPACE, WHICH WILL BE ADDED THE FIRST
    // TIME A NEW Course IS CREATED OR LOADED
    private void initWindow(String windowTitle) {
        // SET THE WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();

        // AND USE IT TO SIZE THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());

        // ADD THE TOOLBAR ONLY, NOTE THAT THE WORKSPACE
        // HAS BEEN CONSTRUCTED, BUT WON'T BE ADDED UNTIL
        // THE USER STARTS EDITING A COURSE
        csbPane = new BorderPane();
        csbPane.setTop(fileToolbarPane);
        primaryScene = new Scene(csbPane);

        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
        // WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
        primaryScene.getStylesheets().add(PRIMARY_STYLE_SHEET);
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }

    // INIT ALL THE EVENT HANDLERS
    private void initEventHandlers() throws IOException {
        // FIRST THE FILE CONTROLS
        fileController = new FileController(messageDialog, yesNoCancelDialog, progressDialog, courseFileManager, siteExporter);
        newCourseButton.setOnAction(e -> {
            fileController.handleNewCourseRequest(this);
        });
        loadCourseButton.setOnAction(e -> {
            fileController.handleLoadCourseRequest(this);
        });
        saveCourseButton.setOnAction(e -> {
            fileController.handleSaveCourseRequest(this, dataManager.getCourse());
        });
        exportSiteButton.setOnAction(e -> {
            fileController.handleExportCourseRequest(this);
        });
        exitButton.setOnAction(e -> {
            fileController.handleExitRequest(this);
        });

        // THEN THE COURSE EDITING CONTROLS
       

        // TEXT FIELDS HAVE A DIFFERENT WAY OF LISTENING FOR TEXT CHANGES
       

        // THE DATE SELECTION ONES HAVE PARTICULAR CONCERNS, AND SO
        // GO THROUGH A DIFFERENT METHOD
       
        
       
    }

    // REGISTER THE EVENT LISTENER FOR A TEXT FIELD
    private void registerTextFieldController(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            courseController.handleCourseChangeRequest(this);
        });
    }
    
    // INIT A BUTTON AND ADD IT TO A CONTAINER IN A TOOLBAR
    private Button initChildButton(Pane toolbar, CSB_PropertyType icon, CSB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        Button button = new Button();
        button.setDisable(disabled);
        button.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        button.setTooltip(buttonTooltip);
        toolbar.getChildren().add(button);
        return button;
    }
     private TextField initChildTextField(Pane toolbar, CSB_PropertyType icon, CSB_PropertyType tooltip, boolean disabled) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imagePath = "file:" + PATH_IMAGES + props.getProperty(icon.toString());
        Image buttonImage = new Image(imagePath);
        TextField textfield = new TextField();
        textfield.setDisable(disabled);
        //textfield.setGraphic(new ImageView(buttonImage));
        Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
        textfield.setTooltip(buttonTooltip);
        toolbar.getChildren().add(textfield);
        return textfield;
    }
    
    // INIT A LABEL AND SET IT'S STYLESHEET CLASS
    private Label initLabel(CSB_PropertyType labelProperty, String styleClass) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String labelText = props.getProperty(labelProperty);
        Label label = new Label(labelText);
        label.getStyleClass().add(styleClass);
        return label;
    }

    // INIT A LABEL AND PLACE IT IN A GridPane INIT ITS PROPER PLACE
    private Label initGridLabel(GridPane container, CSB_PropertyType labelProperty, String styleClass, int col, int row, int colSpan, int rowSpan) {
        Label label = initLabel(labelProperty, styleClass);
        container.add(label, col, row, colSpan, rowSpan);
        return label;
    }

    // INIT A LABEL AND PUT IT IN A TOOLBAR
    private Label initChildLabel(Pane container, CSB_PropertyType labelProperty, String styleClass) {
        Label label = initLabel(labelProperty, styleClass);
        container.getChildren().add(label);
        return label;
    }

    // INIT A COMBO BOX AND PUT IT IN A GridPane
    private ComboBox initGridComboBox(GridPane container, int col, int row, int colSpan, int rowSpan) throws IOException {
        ComboBox comboBox = new ComboBox();
        container.add(comboBox, col, row, colSpan, rowSpan);
        return comboBox;
    }

    // LOAD THE COMBO BOX TO HOLD Course SUBJECTS
   

    // INIT A TEXT FIELD AND PUT IT IN A GridPane
    private TextField initGridTextField(GridPane container, int size, String initText, boolean editable, int col, int row, int colSpan, int rowSpan) {
        TextField tf = new TextField();
        tf.setPrefColumnCount(size);
        tf.setText(initText);
        tf.setEditable(editable);
        container.add(tf, col, row, colSpan, rowSpan);
        return tf;
    }

    // INIT A DatePicker AND PUT IT IN A GridPane
    private DatePicker initGridDatePicker(GridPane container, int col, int row, int colSpan, int rowSpan) {
        DatePicker datePicker = new DatePicker();
        container.add(datePicker, col, row, colSpan, rowSpan);
        return datePicker;
    }

    // INIT A CheckBox AND PUT IT IN A TOOLBAR
    private CheckBox initChildCheckBox(Pane container, String text) {
        CheckBox cB = new CheckBox(text);
        container.getChildren().add(cB);
        return cB;
    }

    // INIT A DatePicker AND PUT IT IN A CONTAINER
    private DatePicker initChildDatePicker(Pane container) {
        DatePicker dp = new DatePicker();
        container.getChildren().add(dp);
        return dp;
    }
    
    // LOADS CHECKBOX DATA INTO A Course OBJECT REPRESENTING A CoursePage
    private void updatePageUsingCheckBox(CheckBox cB, Course course, CoursePage cP) {
        if (cB.isSelected()) {
            course.selectPage(cP);
        } else {
            course.unselectPage(cP);
        }
    }    
}
