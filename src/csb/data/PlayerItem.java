package csb.data;

import java.time.LocalDate;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.DoubleProperty;
import java.lang.Object;

/**
 *
 * @author McKillaGorilla
 */
public class PlayerItem implements Comparable {
    StringProperty first;
    StringProperty last;
    StringProperty proTeam;
    final StringProperty position;
    
    final IntegerProperty birthYear;
    final IntegerProperty rw;
    final IntegerProperty hr;
    final IntegerProperty rbi;
    final DoubleProperty sb;
    final DoubleProperty ba;
    final DoubleProperty estimated;
    final StringProperty note;
    final StringProperty nation;
    
    
    //public static final String DEFAULT_DESCRIPTION = "<ENTER DESCRIPTION>";
   // public static final String DEFAULT_URL = "http://www.google.com";    
    
    public PlayerItem() {
        first = new SimpleStringProperty();
        last = new SimpleStringProperty();
        proTeam = new SimpleStringProperty();
        position = new SimpleStringProperty();
        birthYear = new SimpleIntegerProperty();
        rw = new SimpleIntegerProperty();
        hr = new SimpleIntegerProperty();
        rbi = new SimpleIntegerProperty();
        sb = new SimpleDoubleProperty();
        ba = new SimpleDoubleProperty();
        estimated = new SimpleDoubleProperty();
        note = new SimpleStringProperty();
        nation = new SimpleStringProperty();
        
    }
    
    public void reset() {
       // setDescription();    public void reset() {

      
       // setLink(DEFAULT_URL);
    }
    
    public String getFirst() {
        return first.get();
    }
    
    public void setFirst(String initDescription) {
        first.set(initDescription);
    }
    
    public StringProperty firstProperty() {
        return first;
    }
    
    
    public String getLast() {
        return last.get();
    }
    
    public void setLast(String initDescription) {
       last.set(initDescription);
    }
    
    public StringProperty LastProperty() {
        return last;
    }
    
    public String getProTeam() {
        return proTeam.get();
    }
    
    public void setProTeam(String initDescription) {
        proTeam.set(initDescription);
    }
    
    public StringProperty proTeamProperty() {
        return proTeam;
    }
    
    public String getPosition() {
        return position.get();
    }
    
    public void setPosition(String initDescription) {
        position.set(initDescription);
    }
    
    public StringProperty positionProperty() {
        return position;
    }
    
    public Integer getBirthYear() {
        return birthYear.get();
    }
    
    public void setBirthYear(Integer initDescription) {
        birthYear.set(initDescription);
    }
    
    public IntegerProperty birthYearProperty() {
        return birthYear;
    }
    
    public Integer getRw() {
        return rw.get();
    }
    
    public void setRw(Integer initDescription) {
        rw.set(initDescription);
    }
    
    public IntegerProperty rwProperty() {
        return rw;
    }
    
    public Integer getHr() {
        return hr.get();
    }
    
    public void setHr(Integer initDescription) {
        hr.set(initDescription);
    }
    
    public IntegerProperty hr() {
        return hr;
    }
    
    public Integer getRbi() {
        return rbi.get();
    }
    
    public void setRbi(Integer initDescription) {
        rbi.set(initDescription);
    }
    
    public IntegerProperty rbiProperty() {
        return rbi;
    }
    
    public Double getSb() {
        return sb.get();
    }
    
    public void setSb(Double initDescription) {
        sb.set(initDescription);
    }
    
    public DoubleProperty sbProperty() {
        return sb;
    }
    
    public Double getBa() {
        return ba.get();
    }
    
    public void setBa(Double initDescription) {
        ba.set(initDescription);
    }
    
    public DoubleProperty baProperty() {
        return ba;
    }
    
    public Double getEstimated() {
        return estimated.get();
    }
    
    public void setEstimated(Double initDescription) {
        estimated.set(initDescription);
    }
    
    public DoubleProperty estimatedProperty() {
        return estimated;
    }
    
    public String getNote() {
        return note.get();
    }
    
    public void setNote(String initLink) {
        note.set(initLink);
    }
    
    public StringProperty noteProperty() {
        return note;
    }    
     public String getNation() {
        return nation.get();
    }
    
    public void setNation(String initLink) {
        nation.set(initLink);
    }
    
    public StringProperty nationProperty() {
        return nation;
    }    
    @Override
    public int compareTo(Object obj) {
        PlayerItem otherItem = (PlayerItem)obj;
        return getBa().compareTo(otherItem.getBa());
    }
}
